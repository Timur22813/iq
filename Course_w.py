import sys
from PyQt6.QtWidgets import QWidget, QTabWidget, QDialog, QLabel, QPushButton, QRadioButton, QHBoxLayout, QVBoxLayout, \
    QFormLayout, QLayout, QApplication, QDialogButtonBox, QTabBar, QStatusBar, QMainWindow, QBoxLayout
from PyQt6 import QtGui, QtWidgets, QtMultimediaWidgets, QtMultimedia, QtPdfWidgets, QtPdf, QtSvg, QtSvgWidgets, \
    QtQuickWidgets, QtCore
import time
import threading
from datetime import datetime

from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, MetaData, DateTime, Table, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("IQ-Test")
        MainWindow.setFixedSize(1280, 800)

        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        self.KnowIQ = QtWidgets.QPushButton(self.centralwidget)
        self.KnowIQ.setGeometry(QtCore.QRect(510, 687, 151, 41))
        self.KnowIQ.setObjectName("KnowIQ")
        self.label_res = QtWidgets.QLabel(self.centralwidget)
        self.label_res.setGeometry(QtCore.QRect(660, 700, 55, 16))
        self.label_res.setText("")
        self.label_res.setObjectName("label_res")
        self.Q8 = QtWidgets.QTabWidget(self.centralwidget)
        self.Q8.setGeometry(QtCore.QRect(0, 480, 171, 251))
        self.Q8.setObjectName("Q8")
        self.tab = QtWidgets.QWidget()
        self.countiq = []
        self.sumiq1 = sum(self.countiq)
        self.tab.setObjectName("tab")
        self.label_24 = QtWidgets.QLabel(self.tab)
        self.label_24.setGeometry(QtCore.QRect(0, 10, 161, 16))
        self.label_24.setStyleSheet("font: 75 8pt \"MS Shell Dlg 2\";")
        self.label_24.setObjectName("label_24")
        self.label_25 = QtWidgets.QLabel(self.tab)
        self.label_25.setGeometry(QtCore.QRect(10, 30, 131, 16))
        self.label_25.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_25.setObjectName("label_25")
        self.label_26 = QtWidgets.QLabel(self.tab)
        self.label_26.setGeometry(QtCore.QRect(10, 50, 111, 16))
        self.label_26.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_26.setObjectName("label_26")
        self.label_27 = QtWidgets.QLabel(self.tab)
        self.label_27.setGeometry(QtCore.QRect(10, 70, 131, 16))
        self.label_27.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_27.setObjectName("label_27")
        self.Q8.addTab(self.tab, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.TrueBtn8 = QtWidgets.QPushButton(self.tab_2)
        self.TrueBtn8.setGeometry(QtCore.QRect(0, 80, 93, 28))
        self.TrueBtn8.setObjectName("TrueBtn8")
        self.pushButton_4 = QtWidgets.QPushButton(self.tab_2)
        self.pushButton_4.setGeometry(QtCore.QRect(0, 50, 93, 28))
        self.pushButton_4.setObjectName("pushButton_4")
        self.FalseBtn15 = QtWidgets.QPushButton(self.tab_2)
        self.FalseBtn15.setGeometry(QtCore.QRect(0, 10, 93, 28))
        self.FalseBtn15.setObjectName("FalseBtn15")
        self.Q8.addTab(self.tab_2, "")
        self.Q2 = QtWidgets.QTabWidget(self.centralwidget)
        self.Q2.setGeometry(QtCore.QRect(180, 220, 171, 261))
        self.Q2.setObjectName("Q2")
        self.tab_3 = QtWidgets.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.label_3 = QtWidgets.QLabel(self.tab_3)
        self.label_3.setGeometry(QtCore.QRect(10, 10, 161, 31))
        self.label_3.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_3.setObjectName("label_3")
        self.label_8 = QtWidgets.QLabel(self.tab_3)
        self.label_8.setGeometry(QtCore.QRect(10, 40, 141, 21))
        self.label_8.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_8.setObjectName("label_8")
        self.Q2.addTab(self.tab_3, "")
        self.tab_4 = QtWidgets.QWidget()
        self.tab_4.setObjectName("tab_4")
        self.FalseBtn3 = QtWidgets.QPushButton(self.tab_4)
        self.FalseBtn3.setGeometry(QtCore.QRect(10, 10, 93, 28))
        self.FalseBtn3.setObjectName("FalseBtn3")
        self.TrueBtn2 = QtWidgets.QPushButton(self.tab_4)
        self.TrueBtn2.setGeometry(QtCore.QRect(10, 40, 93, 28))
        self.TrueBtn2.setObjectName("TrueBtn2")
        self.pushButton_5 = QtWidgets.QPushButton(self.tab_4)
        self.pushButton_5.setGeometry(QtCore.QRect(10, 70, 93, 28))
        self.pushButton_5.setObjectName("pushButton_5")
        self.Q2.addTab(self.tab_4, "")
        self.Q3 = QtWidgets.QTabWidget(self.centralwidget)
        self.Q3.setGeometry(QtCore.QRect(360, 220, 171, 261))
        self.Q3.setObjectName("Q3")
        self.tab_5 = QtWidgets.QWidget()
        self.tab_5.setObjectName("tab_5")
        self.label_11 = QtWidgets.QLabel(self.tab_5)
        self.label_11.setGeometry(QtCore.QRect(0, 50, 171, 31))
        self.label_11.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_11.setObjectName("label_11")
        self.label_12 = QtWidgets.QLabel(self.tab_5)
        self.label_12.setGeometry(QtCore.QRect(0, 70, 151, 41))
        self.label_12.setStyleSheet("font: 75 8pt \"MS Shell Dlg 2\";")
        self.label_12.setObjectName("label_12")
        self.Q3.addTab(self.tab_5, "")
        self.tab_6 = QtWidgets.QWidget()
        self.tab_6.setObjectName("tab_6")
        self.FalseBtn5 = QtWidgets.QPushButton(self.tab_6)
        self.FalseBtn5.setGeometry(QtCore.QRect(10, 10, 93, 28))
        self.FalseBtn5.setObjectName("FalseBtn5")
        self.Truebtn3 = QtWidgets.QPushButton(self.tab_6)
        self.Truebtn3.setGeometry(QtCore.QRect(10, 50, 93, 28))
        self.Truebtn3.setObjectName("Truebtn3")
        self.pushButton_6 = QtWidgets.QPushButton(self.tab_6)
        self.pushButton_6.setGeometry(QtCore.QRect(10, 90, 93, 28))
        self.pushButton_6.setObjectName("pushButton_6")
        self.Q3.addTab(self.tab_6, "")
        self.Q4 = QtWidgets.QTabWidget(self.centralwidget)
        self.Q4.setGeometry(QtCore.QRect(540, 220, 171, 261))
        self.Q4.setObjectName("Q4")
        self.tab_7 = QtWidgets.QWidget()
        self.tab_7.setObjectName("tab_7")
        self.label_13 = QtWidgets.QLabel(self.tab_7)
        self.label_13.setGeometry(QtCore.QRect(0, 20, 141, 16))
        self.label_13.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_13.setObjectName("label_13")
        self.label_14 = QtWidgets.QLabel(self.tab_7)
        self.label_14.setGeometry(QtCore.QRect(0, 50, 161, 16))
        self.label_14.setStyleSheet("font: 75 8pt \"MS Shell Dlg 2\";")
        self.label_14.setObjectName("label_14")
        self.label_15 = QtWidgets.QLabel(self.tab_7)
        self.label_15.setGeometry(QtCore.QRect(0, 80, 151, 16))
        self.label_15.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_15.setObjectName("label_15")
        self.label_16 = QtWidgets.QLabel(self.tab_7)
        self.label_16.setGeometry(QtCore.QRect(0, 110, 161, 16))
        self.label_16.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_16.setObjectName("label_16")
        self.Q4.addTab(self.tab_7, "")
        self.tab_8 = QtWidgets.QWidget()
        self.tab_8.setObjectName("tab_8")
        self.FalsBtn7 = QtWidgets.QPushButton(self.tab_8)
        self.FalsBtn7.setGeometry(QtCore.QRect(10, 10, 93, 28))
        self.FalsBtn7.setObjectName("FalsBtn7")
        self.TrueBtn4 = QtWidgets.QPushButton(self.tab_8)
        self.TrueBtn4.setGeometry(QtCore.QRect(10, 40, 93, 28))
        self.TrueBtn4.setObjectName("TrueBtn4")
        self.FalseBtn8 = QtWidgets.QPushButton(self.tab_8)
        self.FalseBtn8.setGeometry(QtCore.QRect(10, 70, 93, 28))
        self.FalseBtn8.setObjectName("FalseBtn8")
        self.Q4.addTab(self.tab_8, "")
        self.Q5 = QtWidgets.QTabWidget(self.centralwidget)
        self.Q5.setGeometry(QtCore.QRect(720, 220, 171, 261))
        self.Q5.setObjectName("Q5")
        self.tab_9 = QtWidgets.QWidget()
        self.tab_9.setObjectName("tab_9")
        self.label_17 = QtWidgets.QLabel(self.tab_9)
        self.label_17.setGeometry(QtCore.QRect(0, 20, 141, 31))
        self.label_17.setStyleSheet("font: 75 10pt \"MS Shell Dlg 2\";")
        self.label_17.setObjectName("label_17")
        self.label_18 = QtWidgets.QLabel(self.tab_9)
        self.label_18.setGeometry(QtCore.QRect(10, 70, 121, 41))
        self.label_18.setStyleSheet("font: 75 10pt \"MS Shell Dlg 2\";")
        self.label_18.setObjectName("label_18")
        self.Q5.addTab(self.tab_9, "")
        self.tab_10 = QtWidgets.QWidget()
        self.tab_10.setObjectName("tab_10")
        self.TrueBtn5 = QtWidgets.QPushButton(self.tab_10)
        self.TrueBtn5.setGeometry(QtCore.QRect(0, 100, 93, 28))
        self.TrueBtn5.setObjectName("TrueBtn5")
        self.FalseBTn9 = QtWidgets.QPushButton(self.tab_10)
        self.FalseBTn9.setGeometry(QtCore.QRect(0, 60, 93, 28))
        self.FalseBTn9.setObjectName("FalseBTn9")
        self.pushButton_7 = QtWidgets.QPushButton(self.tab_10)
        self.pushButton_7.setGeometry(QtCore.QRect(0, 10, 93, 28))
        self.pushButton_7.setObjectName("pushButton_7")
        self.Q5.addTab(self.tab_10, "")
        self.Q6 = QtWidgets.QTabWidget(self.centralwidget)
        self.Q6.setGeometry(QtCore.QRect(900, 220, 171, 261))
        self.Q6.setObjectName("Q6")
        self.tab_11 = QtWidgets.QWidget()
        self.tab_11.setObjectName("tab_11")
        self.label_19 = QtWidgets.QLabel(self.tab_11)
        self.label_19.setGeometry(QtCore.QRect(0, 10, 141, 16))
        self.label_19.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_19.setObjectName("label_19")
        self.label_20 = QtWidgets.QLabel(self.tab_11)
        self.label_20.setGeometry(QtCore.QRect(0, 35, 131, 21))
        self.label_20.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_20.setObjectName("label_20")
        self.label_21 = QtWidgets.QLabel(self.tab_11)
        self.label_21.setGeometry(QtCore.QRect(0, 70, 171, 16))
        self.label_21.setStyleSheet("font: 75 8pt \"MS Shell Dlg 2\";")
        self.label_21.setObjectName("label_21")
        self.Q6.addTab(self.tab_11, "")
        self.tab_12 = QtWidgets.QWidget()
        self.tab_12.setObjectName("tab_12")
        self.TrueBTn6 = QtWidgets.QPushButton(self.tab_12)
        self.TrueBTn6.setGeometry(QtCore.QRect(0, 100, 93, 28))
        self.TrueBTn6.setObjectName("TrueBTn6")
        self.FalseBtn11 = QtWidgets.QPushButton(self.tab_12)
        self.FalseBtn11.setGeometry(QtCore.QRect(0, 50, 93, 28))
        self.FalseBtn11.setObjectName("FalseBtn11")
        self.pushButton_8 = QtWidgets.QPushButton(self.tab_12)
        self.pushButton_8.setGeometry(QtCore.QRect(0, 10, 93, 28))
        self.pushButton_8.setObjectName("pushButton_8")
        self.Q6.addTab(self.tab_12, "")
        self.Q7 = QtWidgets.QTabWidget(self.centralwidget)
        self.Q7.setGeometry(QtCore.QRect(1080, 220, 171, 261))
        self.Q7.setObjectName("Q7")
        self.tab_13 = QtWidgets.QWidget()
        self.tab_13.setObjectName("tab_13")
        self.label = QtWidgets.QLabel(self.tab_13)
        self.label.setGeometry(QtCore.QRect(0, 20, 151, 16))
        self.label.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label.setObjectName("label")
        self.label_22 = QtWidgets.QLabel(self.tab_13)
        self.label_22.setGeometry(QtCore.QRect(0, 50, 181, 20))
        self.label_22.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_22.setObjectName("label_22")
        self.label_23 = QtWidgets.QLabel(self.tab_13)
        self.label_23.setGeometry(QtCore.QRect(0, 80, 161, 31))
        self.label_23.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_23.setObjectName("label_23")
        self.Q7.addTab(self.tab_13, "")
        self.tab_14 = QtWidgets.QWidget()
        self.tab_14.setObjectName("tab_14")
        self.TrueBtn7 = QtWidgets.QPushButton(self.tab_14)
        self.TrueBtn7.setGeometry(QtCore.QRect(0, 100, 93, 28))
        self.TrueBtn7.setObjectName("TrueBtn7")
        self.FalseBtn13 = QtWidgets.QPushButton(self.tab_14)
        self.FalseBtn13.setGeometry(QtCore.QRect(0, 60, 93, 28))
        self.FalseBtn13.setObjectName("FalseBtn13")
        self.pushButton_9 = QtWidgets.QPushButton(self.tab_14)
        self.pushButton_9.setGeometry(QtCore.QRect(0, 20, 93, 28))
        self.pushButton_9.setObjectName("pushButton_9")
        self.Q7.addTab(self.tab_14, "")
        self.Q9 = QtWidgets.QTabWidget(self.centralwidget)
        self.Q9.setGeometry(QtCore.QRect(170, 480, 171, 251))
        self.Q9.setObjectName("Q9")
        self.tab_15 = QtWidgets.QWidget()
        self.tab_15.setObjectName("tab_15")
        self.label_28 = QtWidgets.QLabel(self.tab_15)
        self.label_28.setGeometry(QtCore.QRect(0, 16, 151, 20))
        self.label_28.setStyleSheet("font: 75 8pt \"MS Shell Dlg 2\";")
        self.label_28.setObjectName("label_28")
        self.label_29 = QtWidgets.QLabel(self.tab_15)
        self.label_29.setGeometry(QtCore.QRect(0, 50, 191, 16))
        self.label_29.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_29.setObjectName("label_29")
        self.Q9.addTab(self.tab_15, "")
        self.tab_16 = QtWidgets.QWidget()
        self.tab_16.setObjectName("tab_16")
        self.TrueBtn9 = QtWidgets.QPushButton(self.tab_16)
        self.TrueBtn9.setGeometry(QtCore.QRect(0, 110, 93, 28))
        self.TrueBtn9.setObjectName("TrueBtn9")
        self.pushButton_10 = QtWidgets.QPushButton(self.tab_16)
        self.pushButton_10.setGeometry(QtCore.QRect(0, 60, 131, 28))
        self.pushButton_10.setObjectName("pushButton_10")
        self.pushButton_11 = QtWidgets.QPushButton(self.tab_16)
        self.pushButton_11.setGeometry(QtCore.QRect(0, 20, 93, 28))
        self.pushButton_11.setObjectName("pushButton_11")
        self.Q9.addTab(self.tab_16, "")
        self.Q10 = QtWidgets.QTabWidget(self.centralwidget)
        self.Q10.setGeometry(QtCore.QRect(340, 480, 171, 251))
        self.Q10.setObjectName("Q10")
        self.tab_19 = QtWidgets.QWidget()
        self.tab_19.setObjectName("tab_19")
        self.label_30 = QtWidgets.QLabel(self.tab_19)
        self.label_30.setGeometry(QtCore.QRect(0, 20, 121, 16))
        self.label_30.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_30.setObjectName("label_30")
        self.label_31 = QtWidgets.QLabel(self.tab_19)
        self.label_31.setGeometry(QtCore.QRect(0, 50, 141, 16))
        self.label_31.setStyleSheet("font: 75 8pt \"MS Shell Dlg 2\";")
        self.label_31.setObjectName("label_31")
        self.label_32 = QtWidgets.QLabel(self.tab_19)
        self.label_32.setGeometry(QtCore.QRect(0, 70, 161, 16))
        self.label_32.setStyleSheet("font: 75 8pt \"MS Shell Dlg 2\";")
        self.label_32.setObjectName("label_32")
        self.Q10.addTab(self.tab_19, "")
        self.tab_20 = QtWidgets.QWidget()
        self.tab_20.setObjectName("tab_20")
        self.TrueBtn10 = QtWidgets.QPushButton(self.tab_20)
        self.TrueBtn10.setGeometry(QtCore.QRect(0, 10, 93, 28))
        self.TrueBtn10.setObjectName("TrueBtn10")
        self.FalseBtn19 = QtWidgets.QPushButton(self.tab_20)
        self.FalseBtn19.setGeometry(QtCore.QRect(0, 50, 93, 28))
        self.FalseBtn19.setObjectName("FalseBtn19")
        self.Q10.addTab(self.tab_20, "")
        self.Q1 = QtWidgets.QTabWidget(self.centralwidget)
        self.Q1.setGeometry(QtCore.QRect(0, 220, 171, 261))
        self.Q1.setObjectName("Q1")
        self.tab_17 = QtWidgets.QWidget()
        self.tab_17.setObjectName("tab_17")
        self.label_2 = QtWidgets.QLabel(self.tab_17)
        self.label_2.setGeometry(QtCore.QRect(0, 0, 161, 61))
        self.label_2.setStyleSheet("font: 75 10pt \"MS Shell Dlg 2\";")
        self.label_2.setObjectName("label_2")
        self.label_4 = QtWidgets.QLabel(self.tab_17)
        self.label_4.setGeometry(QtCore.QRect(0, 50, 141, 16))
        self.label_4.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.tab_17)
        self.label_5.setGeometry(QtCore.QRect(0, 70, 141, 20))
        self.label_5.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.tab_17)
        self.label_6.setGeometry(QtCore.QRect(0, 90, 161, 16))
        self.label_6.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_6.setObjectName("label_6")
        self.label_7 = QtWidgets.QLabel(self.tab_17)
        self.label_7.setGeometry(QtCore.QRect(0, 110, 171, 20))
        self.label_7.setStyleSheet("font: 75 9pt \"MS Shell Dlg 2\";")
        self.label_7.setObjectName("label_7")
        self.Q1.addTab(self.tab_17, "")
        self.tab_18 = QtWidgets.QWidget()
        self.tab_18.setObjectName("tab_18")
        self.TrueBtn1 = QtWidgets.QPushButton(self.tab_18)
        self.TrueBtn1.setGeometry(QtCore.QRect(10, 10, 93, 28))
        self.TrueBtn1.setObjectName("TrueBtn1")
        self.FalseBtn1 = QtWidgets.QPushButton(self.tab_18)
        self.FalseBtn1.setGeometry(QtCore.QRect(10, 40, 93, 28))
        self.FalseBtn1.setObjectName("FalseBtn1")
        self.FalseBnt2 = QtWidgets.QPushButton(self.tab_18)
        self.FalseBnt2.setGeometry(QtCore.QRect(10, 70, 93, 28))
        self.FalseBnt2.setObjectName("FalseBnt2")
        self.Q1.addTab(self.tab_18, "")
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setGeometry(QtCore.QRect(0, 60, 81, 21))
        self.label_9.setStyleSheet("font: 75 10pt \"MS Shell Dlg 2\";")
        self.label_9.setObjectName("label_9")
        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setGeometry(QtCore.QRect(0, 100, 81, 16))
        self.label_10.setStyleSheet("font: 75 10pt \"MS Shell Dlg 2\";")
        self.label_10.setObjectName("label_10")
        self.nameEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.nameEdit.setGeometry(QtCore.QRect(40, 51, 151, 31))
        self.nameEdit.setObjectName("nameEdit")
        self.fathernameEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.fathernameEdit.setGeometry(QtCore.QRect(80, 91, 131, 31))
        self.fathernameEdit.setObjectName("FathernameEdit")
        self.okbtn = QtWidgets.QPushButton(self.centralwidget)
        self.okbtn.setGeometry(QtCore.QRect(1150, 750, 93, 28))
        self.okbtn.setObjectName("okbtn")
        self.cancelbtn = QtWidgets.QPushButton(self.centralwidget)
        self.cancelbtn.setGeometry(QtCore.QRect(1060, 750, 93, 28))
        self.cancelbtn.setObjectName("pushButton_2")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.Q8.setCurrentIndex(0)
        self.Q2.setCurrentIndex(0)
        self.Q3.setCurrentIndex(0)
        self.Q4.setCurrentIndex(0)
        self.Q5.setCurrentIndex(0)
        self.Q6.setCurrentIndex(0)
        self.Q7.setCurrentIndex(0)
        self.Q9.setCurrentIndex(1)
        self.Q10.setCurrentIndex(1)
        self.Q1.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "IQ-TESTER"))
        self.label_24.setText(_translate("MainWindow", "Конфеты стоят 44 маната"))
        self.label_25.setText(_translate("MainWindow", "за килограмм."))
        self.label_26.setText(_translate("MainWindow", "Сколько будет"))
        self.label_27.setText(_translate("MainWindow", "Стоить 2,5 кг"))
        self.Q8.setTabText(self.Q8.indexOf(self.tab), _translate("MainWindow", "Tab 1"))
        self.TrueBtn8.setText(_translate("MainWindow", "110"))
        self.pushButton_4.setText(_translate("MainWindow", "56"))
        self.FalseBtn15.setText(_translate("MainWindow", "66"))
        self.Q8.setTabText(self.Q8.indexOf(self.tab_2), _translate("MainWindow", "Tab 2"))
        self.label_3.setText(_translate("MainWindow", "сколько яиц можно"))
        self.label_8.setText(_translate("MainWindow", "сьесть на тощак"))
        self.Q2.setTabText(self.Q2.indexOf(self.tab_3), _translate("MainWindow", "Tab 1"))
        self.FalseBtn3.setText(_translate("MainWindow", "20"))
        self.TrueBtn2.setText(_translate("MainWindow", "1"))
        self.pushButton_5.setText(_translate("MainWindow", "Нисколько"))
        self.Q2.setTabText(self.Q2.indexOf(self.tab_4), _translate("MainWindow", "Tab 2"))
        self.label_11.setText(_translate("MainWindow", "Без 27 минут 8:00 это"))
        self.label_12.setText(_translate("MainWindow", "сколько минут после 5:00?"))
        self.Q3.setTabText(self.Q3.indexOf(self.tab_5), _translate("MainWindow", "Tab 1"))
        self.FalseBtn5.setText(_translate("MainWindow", "177 минут"))
        self.Truebtn3.setText(_translate("MainWindow", "153 минуты"))
        self.pushButton_6.setText(_translate("MainWindow", "163 минуты"))
        self.Q3.setTabText(self.Q3.indexOf(self.tab_6), _translate("MainWindow", "Tab 2"))
        self.label_13.setText(_translate("MainWindow", "Как выглядит число"))
        self.label_14.setText(_translate("MainWindow", "один миллион две тысячи"))
        self.label_15.setText(_translate("MainWindow", "семьсот пятнадцать"))
        self.label_16.setText(_translate("MainWindow", "в обратную сторону?"))
        self.Q4.setTabText(self.Q4.indexOf(self.tab_7), _translate("MainWindow", "Tab 1"))
        self.FalsBtn7.setText(_translate("MainWindow", "517201"))
        self.TrueBtn4.setText(_translate("MainWindow", "5172001"))
        self.FalseBtn8.setText(_translate("MainWindow", "5017201"))
        self.Q4.setTabText(self.Q4.indexOf(self.tab_8), _translate("MainWindow", "Tab 2"))
        self.label_17.setText(_translate("MainWindow", "Сосчитайте в уме:"))
        self.label_18.setText(_translate("MainWindow", "3583 + 5179=?"))
        self.Q5.setTabText(self.Q5.indexOf(self.tab_9), _translate("MainWindow", "Tab 1"))
        self.TrueBtn5.setText(_translate("MainWindow", "8762"))
        self.FalseBTn9.setText(_translate("MainWindow", "8772"))
        self.pushButton_7.setText(_translate("MainWindow", "8552"))
        self.Q5.setTabText(self.Q5.indexOf(self.tab_10), _translate("MainWindow", "Tab 2"))
        self.label_19.setText(_translate("MainWindow", "Батон разрезали "))
        self.label_20.setText(_translate("MainWindow", "на шесть частей"))
        self.label_21.setText(_translate("MainWindow", "Сколько сделали разрезов?"))
        self.Q6.setTabText(self.Q6.indexOf(self.tab_11), _translate("MainWindow", "Tab 1"))
        self.TrueBTn6.setText(_translate("MainWindow", "5"))
        self.FalseBtn11.setText(_translate("MainWindow", "9"))
        self.pushButton_8.setText(_translate("MainWindow", "4"))
        self.Q6.setTabText(self.Q6.indexOf(self.tab_12), _translate("MainWindow", "Tab 2"))
        self.label.setText(_translate("MainWindow", "«Грустный» является"))
        self.label_22.setText(_translate("MainWindow", "противоположным по"))
        self.label_23.setText(_translate("MainWindow", "смыслу слову:"))
        self.Q7.setTabText(self.Q7.indexOf(self.tab_13), _translate("MainWindow", "Tab 1"))
        self.TrueBtn7.setText(_translate("MainWindow", "Весёлый"))
        self.FalseBtn13.setText(_translate("MainWindow", "усталый"))
        self.pushButton_9.setText(_translate("MainWindow", "Тусклый"))
        self.Q7.setTabText(self.Q7.indexOf(self.tab_14), _translate("MainWindow", "Tab 2"))
        self.label_28.setText(_translate("MainWindow", "Слова «канун» и «канон»"))
        self.label_29.setText(_translate("MainWindow", "по смыслу являются:"))
        self.Q9.setTabText(self.Q9.indexOf(self.tab_15), _translate("MainWindow", "Tab 1"))
        self.TrueBtn9.setText(_translate("MainWindow", "Разными"))
        self.pushButton_10.setText(_translate("MainWindow", "Противоположными"))
        self.pushButton_11.setText(_translate("MainWindow", "Одинаковыми"))
        self.Q9.setTabText(self.Q9.indexOf(self.tab_16), _translate("MainWindow", "Tab 2"))
        self.label_30.setText(_translate("MainWindow", "Верно ли то"))
        self.label_31.setText(_translate("MainWindow", "что сокращение «P. S.»"))
        self.label_32.setText(_translate("MainWindow", "означает «постскриптум»?"))
        self.Q10.setTabText(self.Q10.indexOf(self.tab_19), _translate("MainWindow", "Tab 1"))
        self.TrueBtn10.setText(_translate("MainWindow", "ДА"))
        self.FalseBtn19.setText(_translate("MainWindow", "Нет"))
        self.Q10.setTabText(self.Q10.indexOf(self.tab_20), _translate("MainWindow", "Tab 2"))
        self.label_2.setText(_translate("MainWindow", "2012-72,2014-80"))
        self.label_4.setText(_translate("MainWindow", "Какой высоты в см"))
        self.label_5.setText(_translate("MainWindow", "будет дерево"))
        self.label_6.setText(_translate("MainWindow", "если закономерность "))
        self.label_7.setText(_translate("MainWindow", "его роста не изменится"))
        self.Q1.setTabText(self.Q1.indexOf(self.tab_17), _translate("MainWindow", "Tab 1"))
        self.TrueBtn1.setText(_translate("MainWindow", "84"))
        self.FalseBtn1.setText(_translate("MainWindow", "92"))
        self.FalseBnt2.setText(_translate("MainWindow", "98"))
        self.Q1.setTabText(self.Q1.indexOf(self.tab_18), _translate("MainWindow", "Tab 2"))
        self.KnowIQ.setObjectName("KnowIQ")
        self.KnowIQ.setText('Нажми и узнай\nсвой iq')
        self.label_9.setText(_translate("MainWindow", "Имя:"))
        self.label_10.setText(_translate("MainWindow", "Фамилия:"))
        self.okbtn.setText(_translate("MainWindow", "Отправить в БД"))
        self.cancelbtn.setText(_translate("MainWindow", "Закрыть"))
        self.okbtn.clicked.connect(self.okbtnclicked)

    # def okbtnclicked(self):
    #     conn = engine.connect()
    #     ins = user.insert().values(name=self.nameEdit.text(), fathername=self.fathernameEdit.text(), iq=120)
    #     res = conn.execute(ins) beta 1.0
    def okbtnclicked(self):
        conn = engine.connect()
        if sum(self.countiq) == 20:
            ins1 = user.insert().values(name=self.nameEdit.text(), fathername=self.fathernameEdit.text(), iq=20)
            res1 = conn.execute(ins1)
        elif sum(self.countiq) == 40:
            ins2 = user.insert().values(name=self.nameEdit.text(), fathername=self.fathernameEdit.text(), iq=40)
            res2 = conn.execute(ins2)
        elif sum(self.countiq) == 60:
            ins3 = user.insert().values(name=self.nameEdit.text(), fathername=self.fathernameEdit.text(), iq=60)
            res3 = conn.execute(ins3)
        elif sum(self.countiq) == 80:
            ins4 = user.insert().values(name=self.nameEdit.text(), fathername=self.fathernameEdit.text(), iq=80)
            res4 = conn.execute(ins4)
        elif sum(self.countiq) == 100:
            ins5 = user.insert().values(name=self.nameEdit.text(), fathername=self.fathernameEdit.text(), iq=100)
            res5 = conn.execute(ins5)
        elif sum(self.countiq) == 120:
            ins6 = user.insert().values(name=self.nameEdit.text(), fathername=self.fathernameEdit.text(), iq=120)
            res6 = conn.execute(ins6)
        elif sum(self.countiq) == 140:
            ins7 = user.insert().values(name=self.nameEdit.text(), fathername=self.fathernameEdit.text(), iq=140)
            res7 = conn.execute(ins7)
        elif sum(self.countiq) == 160:
            ins8 = user.insert().values(name=self.nameEdit.text(), fathername=self.fathernameEdit.text(), iq=160)
            res8 = conn.execute(ins8)
        elif sum(self.countiq) == 180:
            ins9 = user.insert().values(name=self.nameEdit.text(), fathername=self.fathernameEdit.text(), iq=180)
            res9 = conn.execute(ins9)
        elif sum(self.countiq) == 200:
            ins10 = user.insert().values(name=self.nameEdit.text(), fathername=self.fathernameEdit.text(),iq =200)
            res10 = conn.execute(ins10)
        else:
            print('Error')






metadata = MetaData()
engine = create_engine("mysql+pymysql://root:root@127.0.0.1/timadb")
engine.connect()


user = Table('users', metadata,
             Column('id', Integer(), primary_key=True),
             Column('name', String(30), nullable=False),
             Column('fathername', String(30), nullable=False),
             Column('iq', Integer(), nullable=False),
             )


metadata.create_all(engine)


class MyWin(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()

        self.ui = Ui_MainWindow()  # Экземпляр класса Ui_MainWindow, в нем конструктор всего GUI.
        self.ui.setupUi(self)  # Инициализация GUI

        self.ui.cancelbtn.clicked.connect(self.cancelbtncliked)
        self.ui.KnowIQ.clicked.connect(self.all_iq)

        self.ui.TrueBtn1.clicked.connect(self.tr1)
        self.ui.TrueBtn2.clicked.connect(self.tr1)
        self.ui.Truebtn3.clicked.connect(self.tr1)
        self.ui.TrueBtn4.clicked.connect(self.tr1)
        self.ui.TrueBtn5.clicked.connect(self.tr1)
        self.ui.TrueBTn6.clicked.connect(self.tr1)
        self.ui.TrueBtn7.clicked.connect(self.tr1)
        self.ui.TrueBtn8.clicked.connect(self.tr1)
        self.ui.TrueBtn9.clicked.connect(self.tr1)
        self.ui.TrueBtn10.clicked.connect(self.tr1)

    def tr1(self):
        self.ui.countiq.append(20)

    def all_iq(self):
        if sum(self.ui.countiq) == 0:
            self.ui.label_res.setText('---0 IQ---')
        elif sum(self.ui.countiq) == 20:
            self.ui.label_res.setText('---20 IQ---')
        elif sum(self.ui.countiq) == 40:
            self.ui.label_res.setText('---40 IQ---')
        elif sum(self.ui.countiq) == 60:
            self.ui.label_res.setText('---60 IQ---')
        elif sum(self.ui.countiq) == 80:
            self.ui.label_res.setText('---80 IQ---')
        elif sum(self.ui.countiq) == 100:
            self.ui.label_res.setText('---100 IQ---')
        elif sum(self.ui.countiq) == 120:
            self.ui.label_res.setText('---120 IQ---')
        elif sum(self.ui.countiq) == 140:
            self.ui.label_res.setText('---140 IQ---')
        elif sum(self.ui.countiq) == 160:
            self.ui.label_res.setText('---160 IQ---')
        elif sum(self.ui.countiq) == 180:
            self.ui.label_res.setText('---180 IQ---')
        elif sum(self.ui.countiq) == 200:
            self.ui.label_res.setText('---200 IQ---')
        else:
            print('Просил же 2 раза в одно место не жать :(')




    def cancelbtncliked(self):
        window.close()




Base = declarative_base()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    window = MyWin()
    t1 = threading.Thread(target=Ui_MainWindow)
    t1.start()
    t2 = threading.Thread(target=MyWin)
    t2.start()
    print(threading.active_count())
    window.show()
    sys.exit(app.exec())
